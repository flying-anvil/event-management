<?php

use FlyingAnvil\EventManager\Bus\EventBus;
use FlyingAnvil\EventManager\Collection\Factory\EventListenerRegistrarFactory;
use FlyingAnvil\EventManager\Event\DefaultEvent;
use FlyingAnvil\EventManager\ManualTests\Listener;

require_once __DIR__ . '/../../vendor/autoload.php';

$listener  = new Listener();
$registrar = EventListenerRegistrarFactory::buildFromObjects([
    $listener
]);

$bus = new EventBus($registrar);
$bus->dispenseEvent(DefaultEvent::create());
