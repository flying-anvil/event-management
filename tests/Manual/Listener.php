<?php

declare(strict_types=1);

namespace FlyingAnvil\EventManager\ManualTests;

use FlyingAnvil\EventManager\Collection\EventListenerRegistrar;
use FlyingAnvil\EventManager\Event\DefaultEvent;
use FlyingAnvil\EventManager\Event\EventInterface;
use FlyingAnvil\EventManager\Listener\EventListenerInterface;
use FlyingAnvil\EventManager\Listener\Listen;

class Listener implements EventListenerInterface
{
    public function register(EventListenerRegistrar $registrar): void
    {
        $registrar->listenTo(
            Listen::create(DefaultEvent::class, [$this, 'handleEvent'], Listen::PRIORITY_CRITICAL),
        );
    }

    public function handleEvent(EventInterface $event): void
    {
        var_dump('Listener got Event: ' . get_class($event));
        echo PHP_EOL;
        var_dump($event);
    }
}
