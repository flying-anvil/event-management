<?php

declare(strict_types=1);

namespace FlyingAnvil\EventManager\Listener;

use FlyingAnvil\EventManager\Collection\EventListenerRegistrar;

interface EventListenerInterface
{
    public function register(EventListenerRegistrar $registrar): void;
}
