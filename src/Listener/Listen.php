<?php

declare(strict_types=1);

namespace FlyingAnvil\EventManager\Listener;

class Listen
{
    public const PRIORITY_TRIVIAL   = 10;
    public const PRIORITY_VERY_LOW  = 100;
    public const PRIORITY_LOWER     = 200;
    public const PRIORITY_LOW       = 500;
    public const PRIORITY_AVERAGE   = 1000;
    public const PRIORITY_HIGH      = 2000;
    public const PRIORITY_HIGHER    = 4000;
    public const PRIORITY_VERY_HIGH = 6000;
    public const PRIORITY_HIGHEST   = 8000;
    public const PRIORITY_CRITICAL  = 10000;

    public const PRIORITY_DEFAULT = self::PRIORITY_AVERAGE;

    /** @var string */
    private $eventClass;

    /** @var callable */
    private $handler;

    /** @var int */
    private $priority;

    private function __construct(string $eventClass, callable $handler, int $priority)
    {
        $this->eventClass = $eventClass;
        $this->handler    = $handler;
        $this->priority   = $priority;
    }

    /**
     * Listeners with hight priority are called first
     *
     * @param string $eventClass
     * @param callable $handler
     * @param int $priority
     * @return Listen
     */
    public static function create(string $eventClass, callable $handler, int $priority = self::PRIORITY_DEFAULT): self
    {
        return new self($eventClass, $handler, $priority);
    }

    public function getEventClass(): string
    {
        return $this->eventClass;
    }

    public function getHandler(): callable
    {
        return $this->handler;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }
}
