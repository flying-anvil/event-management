<?php

declare(strict_types=1);

namespace FlyingAnvil\EventManager\Event;

class DefaultEvent implements EventInterface
{
    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }
}
