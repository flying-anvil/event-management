<?php

declare(strict_types=1);

namespace FlyingAnvil\EventManager\Bus;

use FlyingAnvil\EventManager\Collection\EventListenerRegistrar;
use FlyingAnvil\EventManager\Event\EventInterface;

class EventBus
{
    /** @var EventListenerRegistrar */
    private $registrar;

    public function __construct(EventListenerRegistrar $registrar)
    {
        $this->registrar = $registrar;
    }

    public function dispenseEvent(EventInterface $event): void
    {
        $eventClass   = get_class($event);
        $eventHandler = $this->registrar->getHandlerForEvent($eventClass);

        foreach ($eventHandler as $handler) {
            $handler($event);
        }
    }
}
