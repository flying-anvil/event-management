<?php

declare(strict_types=1);

namespace FlyingAnvil\EventManager\Collection\Factory;

use FlyingAnvil\EventManager\Collection\EventListenerRegistrar;
use FlyingAnvil\EventManager\Listener\EventListenerInterface;

class EventListenerRegistrarFactory
{
    /** @var self */
    static private $instance;

    /**
     * @param EventListenerInterface[] $listeners
     * @return EventListenerRegistrar
     * @throws \Exception
     */
    public static function buildFromObjects(array $listeners): EventListenerRegistrar
    {
        $registrar = new EventListenerRegistrar();

        foreach ($listeners as $listener) {
            if (!is_object($listener)) {
                throw new \Exception('no object');
            }

            if (!$listener instanceof EventListenerInterface) {
                throw new \Exception('no event listener');
            }

            $listener->register($registrar);
        }

        return $registrar;
    }

    public static function getSingletonInstance(): self
    {
        if (!self::$instance) {
            self::$instance = new EventListenerRegistrar();
        }

        return self::$instance;
    }
}
