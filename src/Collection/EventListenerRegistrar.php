<?php

declare(strict_types=1);

namespace FlyingAnvil\EventManager\Collection;

use FlyingAnvil\EventManager\Event\EventInterface;
use FlyingAnvil\EventManager\Listener\Listen;
use Generator;

class EventListenerRegistrar
{
    /** @var array */
    private $listeners;

    /**
     * @param Listen ...$listeners
     * @throws \Exception
     */
    public function listenTo(Listen ...$listeners): void
    {
        /** @var Listen $listener */
        foreach ($listeners as $listener) {
            $eventClass = $listener->getEventClass();

            if (!is_subclass_of($eventClass, EventInterface::class, true)) {
                // TODO: Throw better exception
                throw new \Exception(sprintf(
                    '"%s" is not an event, you can only listen to events',
                    $eventClass
                ));
            }

            $this->listeners[$eventClass][$listener->getPriority()][] = $listener->getHandler();
        }
    }

    /**
     * @param string $eventClass
     * @return Generator<callable>
     */
    public function getHandlerForEvent(string $eventClass): Generator
    {
        if (!isset($this->listeners[$eventClass])) {
            return [];
        }

        krsort($this->listeners[$eventClass]);

        foreach ($this->listeners[$eventClass] as $priority) {
            foreach ($priority as $handler) {
                yield $handler;
            }
        }
    }
}
